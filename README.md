# NFT-charity
"NFT-charity platform" project
- link to demo https://drive.google.com/drive/folders/1yxsZfK4VYM1DKBNie8AVJzXfl_EKo9Nx?usp=drive_link 
- MVP is implemented to help animals and shelters, but the system of smart contracts is universal, further branching of various directions of charity is planned
- the project was successfully demonstrated at the BNB Chain hackathon and won one of the sponsor bounties in the "NFT" category: https://www.linkedin.com/posts/kseniia-ekshova_bnb-chain-zero2hero-hackathon-powered-by-activity-7072241215852752897-txq2?utm_source=share&utm_medium=member_desktop
- This is MVP - so not all ideas are fully implemented yet ;)

# Project description

NFT-charity is a dApp created to draw attention to charitable foundations, develop web3 charity and make the donation process more transparent and interesting for donors.
- The project allows Charitable Foundations to register and (after verification) create and manage their NTF collections through our interface (where NTFs serve as an alternative to direct donations), providing more opportunities for fundraising.
- Convenient interface for donors - the project aggregates local foundations from anywhere in the world (without problems with currency conversion, etc.), makes the donation process more interactive (receiving NFT for donations that can potentially provide benefits from project partners), allows to help passively ( using a Charitable token, 1% of each transaction goes to the platform bank), make decisions on the transfer of funds from the Platform Bank to charitable purposes (only for trusted users, holders of the soulbound token).
- Charity Opportunities for Influencers and Artists (NFT Charity Auctions).

# Global objective of the platform:
- Transparent local charity 
- Developing a web3 charity community
- Popularization of charity and altruistic movements

# Problems to solve
1. No donations - it is difficult for local small funds to attract donations, there is not enough advertising
2. No aggregator - all foundations and details are on different sites, there is no fund aggregator
3. No targeted support - the sites of major foundations do not allow you to send funds point by point
4. No transparency - charity fundraisers are not always transparent 
5. no trust - people do not always want to give money without getting something in return
6. No impact - people do not feel that they have an impact on the situation
7. no time - people don't want to waste their time on charity

# Our solutions
1. donations - receiving donations from people anywhere in the world, advertising Foundations on the platform
2. Aggregator - the Platform aggregates Funds in one place
3. Targeted support - the ability to help point
4. Transparency - the application is implemented on a Blockchain, which provides full transparency
5. Trust - users receive NFTs for donations, and all Funds are verified
6. Impact - trusted users (Soulbound Token holders) decide where to send funds from the Platform Bank by voting
7. Time - to be a philanthropist simply by using our CharityCoin in partner applications - 1% of each transaction will be donated to the Charity Platform Bank

# Functionality:
1. CharityCoin token (based on ERC20)
- % 1 of all transactions donated to the platform bank (percentage can be customized)
- all settlements on the platform (NFT purchases, auction, etc.) are made with Charity Coin
- for additional token turnover it is necessary to find partners, e.g. blockchain games (including the use of token in other own projects), in which the token will be used

2. Charitable foundations can register on the platform, passing through verification procedure by the platform owner (official documents are obligatory, to avoid fraud) and receive:
- approve (then the fund can get on the platform foundations list, build their dynamic (replenishable) ERC1155 collection and sell their NFTs on the platform)
- declined (application is rejected because of some incompatibilities, it can be corrected and sent again)
- banned (the address is blacklisted and cannot be registered anymore)
Also, a previously successfully registered fund can be added to the black list for any violations, and then it will not be able to sell NFT on our platform for our token
- Fund can customize the price of its NFT independently
- Foundation independently manages its collection (Collection Foundation smart-contract is created when the Foundation creates a collection from the FoundationsList contract)
- if a buyer wants to buy NFTs, it is possible to do so for any offered price (the difference in price goes to the Foundation)
- any amount of NFTs can be bought (if the Foundation has mined them)
- 90% of proceeds from the sale of NFT goes straight to the address provided at registration, 10% is sent to the Charity Platform bank
- the opportunity to receive direct transfers from donors to the address of the Fund

Benefits for foundations 
- advertising, distribution of information and possibility of fundraising from different parts of the world, currency-independent
- Possibility of creating a Fundraising Campaign (for a specific purpose, e.g. urgent surgery, etc.) from the Platform Bank (if the Campaign is selected by vote) or directly from donors.

Benefits for donators (users) 
- Transparency (all transactions are stored in the blockchain)
- the ability to help local foundations without any problems with currency transfers
- proceeds from storing Charity Coin, potentially increasing in value as the Charity Coin platform develops
- potentially benefits from Platform Partners (e.g. discounts on Partner platforms for purchased NFTs, use of Charity Coin on Partner platforms, etc.)

3. Charity Auction
- artists can register on the platform after a simple verification procedure (allowed content to be sold, etc.).
- after that they can put their unique NFT (ERC721, which have some value) for auction
- by default 50% of proceeds from NFT sales are sent to artists, and 50% of proceeds are sent to the Platform bank, but artists can customize this percentage 
and send more than 50% (but no less) to the Platform Bank if they wish.

The goal of the auction is to raise more money for charitable purposes at the Platform Bank

Benefits for artists: 
- Creating a positive reputation among the artists' audience, advertising and attracting new audiences, additional revenue from advertising and NFT sales

Benefits for the Charity Platform:
- advertising and attracting new audiences, additional revenue from advertising and selling NFTs, increased turnover of Charity Coin token

4. Charity Bank and Voting
- donators (users of the Platform) for achievements on the Platform (a system of achievements will be added later) can get into the top donors 
and receive a Soulbound token (Philantropist), which allows to participate in the voting, which will decide where and what amount will be transferred from the Platform Bank (replenished from the commissions of the Token, NFT sales on the platform, direct donations, etc.)
- the voting session is initiated by the platform owner (later it is planned to add the functionality to collect suggestions for voting response options from users, until then, the collection of ideas can be organized, for example, in social networks)
- Achievement system is necessary to avoid fraud, so that the bank could manage the really altruistic donors who contributed to various Funds and Campaigns (so the Funds themselves do not transfer money from the bank for themselves, etc.), as well as an additional gamification to attract and retain users on the platform
- getting achievements involves participation in various activities on the platform, the purchase of NFT different Funds, etc. (as well as potentially for real achievements, such as volunteering)
(while the system of achievements in development + for test purposes to get Soulbound token can a holder of more than 100 Charity Coin)
- the number of Charity Coin Tokens, collected at the moment in the Bank of the Platform, and the number of Tokens, transferred by the results of the votes to the addresses of the Funds and for specific purposes, is displayed on the main page

# By which the platform sustains its existence:
- Direct donations (tax savings can be promoted to businesses) for platform development, investments
- operating costs required to maintain the Platform can be covered from the Platform Bank
- advertising of partners/artists
- collection of small registration fees from foundations and artists
- token swap fee (liquidity add-on)

# Roadmap for the near future:
- finalization and development of the above functionality to a full-fledged product
- allocation of the Administrator role to verify Funds, create voting sessions, etc.
- attracting Funds and Artists
- social media promotion
- attracting Partners 
- fundraising

# Ideas for development:
- Collaboration with projects of other my team members (NFTinder, etc.)
- own ipfs server and/or web-application for storing NFT collections, 
which will allow Foundations and Artists to upload their files directly to our site and create collections immediately,
and to move the registration form for verification of Funds to our site (we are currently switching to the Google form).
- collection is created automatically through the Foundation's personal account (the Foundation uploads files)
- involvement of famous artists and philanthropists for large-scale charity auctions and advertising of the platform
- NFT tied to real/virtual items on partner platforms (web3 games, etc.)
- adding NFT collections with other types of data, in addition to images - music, videos, etc.
- a "Favorites" section for users to store their favorite Fonds, collections, etc.
- user favorites (foundations, collections, etc.)
- events and release of various thematic NFT collections
- special NFT for charitable donations from real life (for volunteers, etc.)

# List of deployed contracts:
Charity Coin 
CharityBank
FoundationStorage 
FoundationsList 
Philanthropist Token (draft version)
AtristList (draft version)
Auction (draft version)
Voting (draft version)

# Instructions

# Foundation Registration (via UI form)
1. For registration and verification, need to fill out a Registration form and Google form (only official Foundations can register)
![Image alt](images/instructions/1.png)
![Image alt](images/instructions/2.png)

2. After owners approval, Foundation can create NFT collection
![Image alt](images/instructions/3.png)
![Image alt](images/instructions/3.5.png)

3. Foundation can manage their NFT collection
![Image alt](images/instructions/4.png)

4. After mint NFTs to shop - it is available to purchase by donators in Foundation List

# Buy CharityCoin and Foundations NFTs
1. For buy NFT Charity Coin is required
![Image alt](images/instructions/5.png)

2. Donator can buy NFT from Foundation List
![Image alt](images/instructions/6.png)


# Draft parts 
# Artist registration
*see contract address above

1. For simple registration, need to fill out a Registration form
![Image alt](images/instructions/7.png)

# Auction (draft version)
*see contract address above

1. After approval (if there is no prohibited content) artist can create custom auction session
![Image alt](images/instructions/8.png)

2. Users can bid during the auction session (approve for transger tokens is requiered)
3. When Artist close auction - a certain percentage (not less than 50%, depending on the owner's setting) of the proceeds is sent to the Platform's Bank address, the rest of the funds - to the artist's account

# Philanthropist Soulbound Token (draft version)
*see contract address above

- User cant get Philanthropist if elligable (for MVP - CharityCoin balance of more than 100 (a system of achievements is planned for a real application))
- it is impossible to transfer or sell the token, only burn it

# Voting (draft/not deployed)

1. Owner can create and start voting session
2. Soulbound Token holders can vote
3. Voting result is define target (foundasion or fundrising campaign) for Charity Bank Tokens transfer









